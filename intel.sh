#!/bin/bash
function askYesNo {
        QUESTION=$1
        DEFAULT=$2
        if [ "$DEFAULT" = true ]; then
                OPTIONS="[Y/n]"
                DEFAULT="y"
            else
                OPTIONS="[y/N]"
                DEFAULT="n"
        fi
        read -p "$QUESTION $OPTIONS " -n 1 -s -r INPUT
        INPUT=${INPUT:-${DEFAULT}}
        echo ${INPUT}
        if [[ "$INPUT" =~ ^[yY]$ ]]; then
            ANSWER=true
        else
            ANSWER=false
        fi
}

askYesNo " This script only supports gpus that are supported in the latest driver. Most cards/cpus after 2014 should work. Would you like to proceed? Default No." false
if [ $ANSWER = true ]; then
    dnf install "$dnf_added_packages_intel" -y
    sleep 5
else
    echo "Not installing drivers"
fi