# Nostos

## Description
A greek word meaning "homecoming". Nostos is a collection of scripts and tools deseigned to improve the fedora experience. To make it a home you can come back to time and time again.

Currently the program is deseigned around KDE plasma. Soon I plan on implementing different DE support starting with gnome. If you run it now on a non-plasma system it will install a few plasma apps, and attempt to remove several apps that I see as pointles. The configurations will also be mostly useless.

## Installation
 `git clone --recursive https://gitlab.com/sirmrgentleman/nostos`

 `cd nostos`

 `chmod +x nostos.sh`

edit `nostos/nostos.d/packages.source` to set custom packages you would like added and removed. 

## Usage
From current director: `./nostos.sh` 


Additionally, you can specifiy a custom packages.source file by running nostos and specifiying the full path to the file when prompted.



## Contributing
Contributions accepted. Bash only. 

## Authors and acknowledgment
Author: sirmrgentleman
Credit to @tduck973564 on github for some of the config changes made to KDE. Wouldn't have known how to golbally enable wayland for Electron apps without him.

