#!/bin/bash
#Plasma Specifics.
function askYesNo {
        QUESTION=$1
        DEFAULT=$2
        if [ "$DEFAULT" = true ]; then
                OPTIONS="[Y/n]"
                DEFAULT="y"
            else
                OPTIONS="[y/N]"
                DEFAULT="n"
        fi
        read -p "$QUESTION $OPTIONS " -n 1 -s -r INPUT
        INPUT=${INPUT:-${DEFAULT}}
        echo ${INPUT}
        if [[ "$INPUT" =~ ^[yY]$ ]]; then
            ANSWER=true
        else
            ANSWER=false
        fi
}


sudo sh -c "touch /etc/xdg/spectaclerc && echo '
    [General]
    clipboardGroup=PostScreenshotCopyImage' >> /etc/xdg/spectaclerc"

askYesNo "Install reccomended KDE packages and remove reccomended packages? Default Yes" true
if [ $ANSWER = true ]; then
    sudo dnf remove $dnf_removed_packages_plasma -y
    sudo dnf install $dnf_added_packages_plasma -y
fi
