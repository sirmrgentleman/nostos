#!/bin/bash

#functions
function askYesNo {
        QUESTION=$1
        DEFAULT=$2
        if [ "$DEFAULT" = true ]; then
                OPTIONS="[Y/n]"
                DEFAULT="y"
            else
                OPTIONS="[y/N]"
                DEFAULT="n"
        fi
        read -p "$QUESTION $OPTIONS " -n 1 -s -r INPUT
        INPUT=${INPUT:-${DEFAULT}}
        echo "${INPUT}"
        if [[ "$INPUT" =~ ^[yY]$ ]]; then
            ANSWER=true
        else
            ANSWER=false
        fi
}

#source some variables.
source /etc/os-release

askYesNo "Would you like to specify a custom package list? Default No." false
if [ $ANSWER = true ]; then
    read -p "Please provide the complete path to your custom package list." -r PACKAGES_SOURCE
else
    PACKAGES_SOURCE=nostos.d/packages.source
fi

source "$PACKAGES_SOURCE"
export dnf_removed_packages_plasma
export dnf_added_packages_plasma
export dnf_removed_packages_nvidia
export dnf_added_packages_nvidia
export dnf_removed_packages_amd
export dnf_added_packages_amd
export dnf_removed_packages_intel
export dnf_added_packages_intel
export flatpak_added_packages

#TODO: Request flags from USER before going with defaults
#DONE. Leaving as an example of the proper flags to use for electron apps in the CLI.
#wanted_universal_electron_flags="--enable-features=UseOzonePlatform --ozone-platform=wayland"

#install packages based on distro
echo "Gathering distro info and preparing for package configuration"
if [ "$ID" = fedora ]; then

    askYesNo "Install VSCode(Microsoft build)? Default Yes." true
    if [ $ANSWER = true ]; then
        sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
        echo -e "[code]\nname=Visual Studio Code\nbaseurl=https://packages.microsoft.com/yumrepos/vscode\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" | sudo tee /etc/yum.repos.d/vscode.repo > /dev/null
        sudo dnf install code -y
        echo "VSCode installed"
    fi
    askYesNo "Configure RPM Fusion and install reccomended packages as specified in $PACKAGES_SOURCE? Default Yes." true
    if [ $ANSWER = true ]; then
        sudo dnf install https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
        sudo dnf config-manager --enable fedora-cisco-openh264 -y
        sudo dnf groupupdate core -y
        sudo dnf remove $dnf_removed_packages -y
        sudo dnf install $dnf_added_packages -y
        sudo dnf swap ffmpeg-free ffmpeg --allowerasing -y
        sudo dnf groupupdate multimedia --setopt="install_weak_deps=False" --exclude=PackageKit-gstreamer-plugin -y
        sudo dnf groupupdate sound-and-video -y
    fi
#TODO: set up automated installs for each gpu type
    askYesNo "Configure flathub as a flatpak source? Default Yes." true
    if [ $ANSWER = true ]; then
        sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
        sudo flatpak remote-modify --enable flathub
    fi
    echo "Programs installed and repos configured"
    sleep 5
else
    echo "Nostos is only supported on Fedora Linux. Exiting...."
    exit
fi

#check if the user wants wayland then set up some wayland specifics.
askYesNo "Enable wayland support? Default No." false
if [ "$ANSWER" = true ]; then
    wayland_enabled=true
    echo "wayland features will be enabled"
    sudo touch /etc/profile.d/electron.sh && sudo echo 'ELECTRON_OZONE_PLATFORM_HINT=auto' | sudo tee /etc/profile.d/electron.sh
else
    echo "wayland features will not be enabled, continuing..."
fi

askYesNo "Install KDE specific global configs and packages? Default Yes." true
if [ $ANSWER = true ]; then
    bash ./plasma.sh
fi

askYesNo "Would you like to install potentially propretary codecs and drivers for your GPU? Default Yes" true
if [ $ANSWER = true ]; then
    read -p "What gpu type do you have? Valid input is 'nvidia' 'amd' or 'intel' without the quotations." -r GPU
    if [ "$GPU" = nvidia ]; then
        bash ./nvidia.sh
    elif [ "$GPU" = amd ]; then
        bash ./amd.sh
    elif [ "$GPU" = intel ]; then
        bash ./intel.sh
    fi
else
    echo "leaving default drivers"
fi