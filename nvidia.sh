#!/bin/bash
function askYesNo {
        QUESTION=$1
        DEFAULT=$2
        if [ "$DEFAULT" = true ]; then
                OPTIONS="[Y/n]"
                DEFAULT="y"
            else
                OPTIONS="[y/N]"
                DEFAULT="n"
        fi
        read -p "$QUESTION $OPTIONS " -n 1 -s -r INPUT
        INPUT=${INPUT:-${DEFAULT}}
        echo ${INPUT}
        if [[ "$INPUT" =~ ^[yY]$ ]]; then
            ANSWER=true
        else
            ANSWER=false
        fi
}

askYesNo " This script only supports gpus that are supported in the latest driver. Most cards after 2014 should work. Would you like to proceed? Default No." false
if [ $ANSWER = true ]; then
    sudo dnf install $dnf_added_packages_nvidia -y
    echo "please do not reboot until akmods have finished being built. THis may take up to 5 minutes."
    echo "Once the module is built, "modinfo -F version nvidia" should output the version of the driver such as 440.64 and not modinfo: ERROR: Module nvidia not found."
    sleep 5
else
    echo "Not installing drivers"
fi